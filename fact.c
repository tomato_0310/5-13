
int fact(int x) {
    int ans = 1;
    if (x < 0){ ans = -1 ; }
    else{
        for (;x != 0;){
            ans = ans * x ;
            x-- ;
        }
    }
    return ans ;
}