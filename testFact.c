/* testfact.c by suzuki */
#include <stdio.h>
#include "fact.h"
#include "testCommon.h"

void testFact() {
    testStart("fact");
    assertEqualsInt(fact(1), 1);
    assertEqualsInt(fact(2), 2);
    assertEqualsInt(fact(3), 6);
    assertEqualsInt(fact(6), 720);
    assertEqualsInt(fact(0), 1);
    assertEqualsInt(fact(-1), -1);
    assertEqualsInt(fact(-3), -1);
}

int main() {
    testFact();
    testErrorCheck(); // この行は絶対に消さないこと
    return 0;
}


/* first 
 testFact.c:7:21: warning: implicit declaration of function 'fact' is invalid in
 C99 [-Wimplicit-function-declaration]
 assertEqualsInt(fact(1), 1);
 ^
 ./testCommon.h:33:51: note: expanded from macro 'assertEqualsInt'
 #define assertEqualsInt(a, b) assertEqualsIntFunc(a, b, __FILE__, __LINE__)
 ^
 1 warning generated.
 cc -o testFact -Wall -g testFact.o fact.o testCommon.o -lm
 Undefined symbols for architecture x86_64:
 "_fact", referenced from:
 _testFact in testFact.o
 ld: symbol(s) not found for architecture x86_64
 clang: error: linker command failed with exit code 1 (use -v to see invocation)
 make: *** [testFact] Error 1
 
*/
/* プロトタイプ
 
 Undefined symbols for architecture x86_64:
 "_fact", referenced from:
 _testFact in testFact.o
 ld: symbol(s) not found for architecture x86_64
 clang: error: linker command failed with exit code 1 (use -v to see invocation)
 make: *** [testFact] Error 1

 */
/*

 make: Nothing to be done for `test'.
 == Test fact ==
 Error in testFact.c(8): a != b (0 != 1)
 ###### Error exist!!!! [# of Tests = 1, # of pass = 0 (0%)] ######
 
*/
/*

 == Test fact ==
 All tests are Ok. [# of Tests = 1, # of pass = 1 (100%)]
 
*/
/*

 == Test fact ==
 Error in testFact.c(9): a != b (1 != 2)
 Error in testFact.c(10): a != b (1 != 6)
 Error in testFact.c(11): a != b (1 != 720)
 ###### Error exist!!!! [# of Tests = 4, # of pass = 1 (25%)] ######
 
*/
/*

 == Test fact ==
 All tests are Ok. [# of Tests = 4, # of pass = 4 (100%)]

*/
/*
 
 == Test fact ==
 Error in testFact.c(13): a != b (1 != -1)
 Error in testFact.c(14): a != b (1 != -1)
 ###### Error exist!!!! [# of Tests = 7, # of pass = 5 (71%)] ######

*/
/*

 == Test fact ==
 All tests are Ok. [# of Tests = 7, # of pass = 7 (100%)]

*/