TARGET=factMain
TEST_TARGET=testFact
FUNC=fact.o
HEADERS=fact.h
MAIN=$(TARGET).o
TEST_MAIN=$(TEST_TARGET).o
TEST_COMMON=testCommon.o
LIBS=-lm
CFLAGS=-Wall -g
RM=rm -f

ifeq ($(OS),Windows_NT)
  CC=gcc
  RM=cmd.exe /C del
endif

$(TARGET): $(MAIN) $(FUNC) $(HEADERS)
	$(CC) -o $@ $(CFLAGS) $(MAIN) $(FUNC) $(LIBS)

test: $(TEST_TARGET)

$(TEST_TARGET): $(TEST_MAIN) $(FUNC) $(TEST_COMMON) $(HEADERS)
	$(CC) -o $@ $(CFLAGS) $(TEST_MAIN) $(FUNC) $(TEST_COMMON) $(LIBS)

clean:
	$(RM) $(TARGET) $(TEST_TARGET) $(TEST_MAIN) $(TEST_COMMON) $(MAIN) $(FUNC)
